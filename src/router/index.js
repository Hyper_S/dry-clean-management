import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path: '/memberManagement',
    component: Layout,
    redirect: '/member',
    name: 'memberManagement/memberCard',
    meta: { title: '会员管理', icon: 'el-icon-s-help' },
    alwaysShow: true,
    children: [
      {
        path: 'member',
        name: 'member',
        component: () => import('@/views/memberManagement/memberCard/index'),
        meta: { title: '会员卡', icon: 'el-icon-user-solid' }
      },
      {
        path: 'memberList',
        name: 'memberList',
        component: () => import('@/views/memberManagement/memberList/index'),
        meta: { title: '会员列表', icon: 'el-icon-user-solid' }
      },
      {
        path: 'memberAccount',
        name: 'memberAccount',
        component: () => import('@/views/memberManagement/memberAccount/index'),
        meta: { title: '会员消费', icon: 'el-icon-user-solid' }
      },
      {
        path: 'memberCharge',
        name: 'memberCharge',
        component: () => import('@/views/memberManagement/memberCharge/index'),
        meta: { title: '会员充值', icon: 'el-icon-user-solid' }
      },
    ],
  },
  {
    path: '/clothesManagement',
    component: Layout,
    redirect: '/clothesManagement/clothesList',
    name: 'clothesManagement',
    meta: { title: '衣品管理', icon: 'el-icon-s-help' },
    alwaysShow: true,
    children: [
      {
        path: 'clothesList',
        name: 'clothesList',
        component: () => import('@/views/clothesManagement/clothesList/index'),
        meta: { title: '衣品分类', icon: 'tree' }
      },
    ]
  },
  {
    path: '/orderManagement',
    component: Layout,
    redirect: '/orderManagement/orderList',
    name: 'orderManagement',
    meta: { title: '订单管理', icon: 'el-icon-s-help' },
    alwaysShow: true,
    children: [
      {
        path: 'order',
        name: 'order',
        component: () => import('@/views/orderManagement/order/index'),
        meta: { title: '开单', icon: 'tree' }
      },
      {
        path: 'orderList',
        name: 'orderList',
        component: () => import('@/views/orderManagement/orderList/index'),
        meta: { title: '订单列表', icon: 'tree' }
      },
    ]
  },
  {
    path: '/dataManagement',
    component: Layout,
    redirect: '/dataManagement/saleStatistics',
    name: 'dataManagement',
    meta: { title: '数据统计', icon: 'el-icon-s-help' },
    alwaysShow: true,
    children: [
      {
        path: 'saleStatistics',
        name: 'saleStatistics',
        component: () => import('@/views/dataManagement/saleStatistics/index'),
        meta: { title: '销售额统计', icon: 'tree' }
      },
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
